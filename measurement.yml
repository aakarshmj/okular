workflow:
  rules:
    # Ensure the pipeline runs for tag pushes
    - if: $CI_COMMIT_TAG
    # Ensure the pipeline runs for web triggers i.e triggered from the web ui
    - if: $CI_PIPELINE_SOURCE == "web"
    # Ensure the pipeline doesn't run for every commit push
    - if: $CI_PIPELINE_SOURCE == "push"
      when: never

variables:
  LABPC_IP: "192.168.170.23" # IP address of the LABPC
  PM_IP: "192.168.170.22" # IP address of the PM (Power Meter)

include:
  - project: sysadmin/ci-utilities
    file:
      - /gitlab-templates/flatpak.yml

kecolab-worker-build:
  stage: publish
  image: alpine
  dependencies:
    - flatpak
  tags:
    - EcoLabWorker
  before_script:
    - echo $CI_PROJECT_NAME:$CI_COMMIT_REF_NAME
  script:
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP "
      flatpak install --user $CI_PROJECT_NAME.flatpak -y "

kecolab-worker-energy_measurement:
  stage: publish
  image: alpine
  dependencies:
    # Run only when kecolab-worker-build is running
    - kecolab-worker-build
  timeout: 12h
  tags:
    - EcoLabWorker
  before_script:
  # Download and Copy Usage scenario scripts to the LABPC
    - git clone --no-checkout https://invent.kde.org/teams/eco/remote-eco-lab.git
    - cd remote-eco-lab
    - git checkout master
    - git config core.sparseCheckout true
    - git sparse-checkout set scripts/test_scripts/org.kde.$CI_PROJECT_NAME
    - scp -o StrictHostKeyChecking=no -r -i ~/.ssh/kecolab scripts/test_scripts/org.kde.$CI_PROJECT_NAME/* kecolab@$LABPC_IP:/tmp/
    - cd .. && rm -rf remote-eco-lab
    # Check for configuration script for application under test
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP 'export DISPLAY=:0 && export TERM=xterm && cd /tmp/ && if [ -f "configuration.sh" ]; then chmod +x configuration.sh && ./configuration.sh; fi; exit'
  script:
    - export CURRENT_DATE=$(date +%Y%m%d)
  # Start taking PM Readings (Script 1)
    - cd /home/gitlab-runner/GUDEPowerMeter && nohup python3 check_gude_modified.py -i 1 -x $PM_IP >> ~/testreadings1.csv 2>/dev/null &
  # Start taking Hardware readings using collectl (for script 1)
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP '
      nohup collectl -s cdmn -i1 -P --sep 59 -f ~/test1.csv >/dev/null 2>&1 &
      export DISPLAY=:0 && export TERM=xterm && cd /tmp/ && chmod +x log_sus.sh && ./log_sus.sh'
  # Kill the Process taking PM Readings
    - pkill -f check_gude_modified.py
  # Start taking PM Readings (Script 2)
    - cd /home/gitlab-runner/GUDEPowerMeter && nohup python3 check_gude_modified.py -i 1 -x $PM_IP >> ~/testreadings2.csv 2>/dev/null & 
  # Start taking Hardware readings using collectl (for script 2)
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP '
      nohup collectl -s cdmn -i1 -P --sep 59 -f ~/test2.csv >/dev/null 2>&1 &
      export DISPLAY=:0 && export TERM=xterm && cd /tmp/ && chmod +x log_baseline.sh && ./log_baseline.sh'
    - pkill -f check_gude_modified.py
  # Start taking PM Readings (Script 3)
    - cd /home/gitlab-runner/GUDEPowerMeter && nohup python3 check_gude_modified.py -i 1 -x $PM_IP >> ~/testreadings3.csv 2>/dev/null &
  # Start taking Hardware readings using collectl (for script 3)
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP '
      nohup collectl -s cdmn -i1 -P --sep 59 -f ~/test3.csv >/dev/null 2>&1 &
      export DISPLAY=:0 && export TERM=xterm && cd /tmp/ && chmod +x log_idle.sh && ./log_idle.sh'
    - pkill -f check_gude_modified.py
  # Export collectl readings using Current Date in the filename
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP "
      export CURRENT_DATE=$(date +%Y%m%d) && cd ~/ && cp test1.csv-kecolab-$CURRENT_DATE.tab.gz ~/test1.csv-kecolab.tab.gz"
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP "
      export CURRENT_DATE=$(date +%Y%m%d) && cd ~/ && cp test2.csv-kecolab-$CURRENT_DATE.tab.gz ~/test2.csv-kecolab.tab.gz"
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP "
      export CURRENT_DATE=$(date +%Y%m%d) && cd ~/ && cp test3.csv-kecolab-$CURRENT_DATE.tab.gz ~/test3.csv-kecolab.tab.gz"
  # Export Power Meter Readings
    - cp ~/testreadings1.csv testreadings1.csv
    - cp ~/testreadings2.csv testreadings2.csv
    - cp ~/testreadings3.csv testreadings3.csv
  # Export all the Raw Power and Hardware Readings to Artifacts
    - scp -o StrictHostKeyChecking=no -r -i  ~/.ssh/kecolab kecolab@$LABPC_IP:~/test1.csv-kecolab.tab.gz test1.csv-kecolab-$CURRENT_DATE.tab.gz
    - scp -o StrictHostKeyChecking=no -r -i  ~/.ssh/kecolab kecolab@$LABPC_IP:~/test2.csv-kecolab.tab.gz test2.csv-kecolab-$CURRENT_DATE.tab.gz
    - scp -o StrictHostKeyChecking=no -r -i  ~/.ssh/kecolab kecolab@$LABPC_IP:~/test3.csv-kecolab.tab.gz test3.csv-kecolab-$CURRENT_DATE.tab.gz
    - scp -o StrictHostKeyChecking=no -r -i  ~/.ssh/kecolab kecolab@$LABPC_IP:~/log_sus.csv log_sus.csv
    - scp -o StrictHostKeyChecking=no -r -i  ~/.ssh/kecolab kecolab@$LABPC_IP:~/log_baseline.csv log_baseline.csv
    - scp -o StrictHostKeyChecking=no -r -i  ~/.ssh/kecolab kecolab@$LABPC_IP:~/log_idle.csv log_idle.csv
  # Remove all the logs 
    - cd ~/ && rm testreadings1.csv testreadings2.csv testreadings3.csv
    - ssh -o StrictHostKeyChecking=no -i ~/.ssh/kecolab kecolab@$LABPC_IP ' export CURRENT_DATE=$(date +%Y%m%d) && rm log_sus.csv log_baseline.csv log_idle.csv test1.csv-kecolab-$CURRENT_DATE.tab.gz test2.csv-kecolab-$CURRENT_DATE.tab.gz test3.csv-kecolab-$CURRENT_DATE.tab.gz test1.csv-kecolab.tab.gz test2.csv-kecolab.tab.gz test3.csv-kecolab.tab.gz && cd /tmp/  && rm log_sus.sh log_baseline.sh log_idle.sh'
  artifacts:
    paths:
      - testreadings1.csv
      - testreadings2.csv
      - testreadings3.csv
      - "test1.csv-kecolab-*.tab.gz"
      - "test2.csv-kecolab-*.tab.gz"
      - "test3.csv-kecolab-*.tab.gz"
      - log_sus.csv
      - log_baseline.csv
      - log_idle.csv

# Result Stage (To Generate Energy Measurement Report)
kecolab-worker-result:
  stage: publish
  image: invent-registry.kde.org/sysadmin/ci-images/kecolab-analysis:latest
  dependencies:
    - kecolab-worker-energy_measurement
  script:
    - export CURRENT_DATE=$(date +%Y%m%d)
    - gunzip test1.csv-kecolab-$CURRENT_DATE.tab.gz
    - gunzip test2.csv-kecolab-$CURRENT_DATE.tab.gz
    - gunzip test3.csv-kecolab-$CURRENT_DATE.tab.gz

    # Preprocess Raw data for OSCAR Script
    - Rscript ~/Preprocessing.R test1.csv-kecolab-$CURRENT_DATE.tab test2.csv-kecolab-$CURRENT_DATE.tab test3.csv-kecolab-$CURRENT_DATE.tab $CI_PROJECT_DIR
    # Run OSCAR Analysis script to generate a report SUS
    - Rscript ~/sus_analysis_script.R
    - cp -r ~/SUS_Report.pdf ~/SUS_Report.tex ~/sus_graphics ~/SUS_Report_files $CI_PROJECT_DIR/
    # Run OSCAR Analysis script to generate a report for Idle Mode
    - Rscript ~/idle_analysis_script.R
    - cp -r ~/Idle_Report.pdf ~/Idle_Report.tex ~/idle_graphics ~/Idle_Report_files $CI_PROJECT_DIR/

  artifacts:
    paths:
     - SUS_Report.pdf
     - SUS_Report.tex
     - SUS_Report_files
     - sus_graphics
     - Idle_Report.pdf
     - Idle_Report.tex
     - Idle_Report_files
     - idle_graphics
